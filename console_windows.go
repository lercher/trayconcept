package main

import (
	"syscall"
)

var (
	getConsoleWindow = syscall.NewLazyDLL("kernel32.dll").NewProc("GetConsoleWindow") // https://docs.microsoft.com/en-us/windows/console/getconsolewindow
	showWindow       = syscall.NewLazyDLL("user32.dll").NewProc("ShowWindow")         // https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-showwindow
	allocConsole     = syscall.NewLazyDLL("kernel32.dll").NewProc("AllocConsole")     // https://docs.microsoft.com/en-us/windows/console/allocconsole
	setActiveWindow  = syscall.NewLazyDLL("user32.dll").NewProc("SetActiveWindow")    // https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-setactivewindow
)

const (
	swHide       = 0
	swShowNormal = 1
	swShow       = 5
	swRestore    = 9
)

func showConsole() {
	hwnd, _, _ := getConsoleWindow.Call()
	if hwnd != 0 {
		showWindow.Call(hwnd, swShowNormal) // needs to run on the UI thread to really activate the window
	}
}

func hideConsole() {
	hwnd, _, _ := getConsoleWindow.Call()
	if hwnd != 0 {
		showWindow.Call(hwnd, swHide)
	}
}
