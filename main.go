package main

import (
	"log"

	"github.com/getlantern/systray"
)

func main() {
	hideConsole()
	log.Println("This is tray concept")
	systray.Run(onReady, onExit)
	log.Println("Done")
}
