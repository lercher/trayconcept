package main

import (
	_ "embed"
	"log"
	"time"

	"github.com/getlantern/systray"
)

//go:embed icon.ico
var icon []byte

func onReady() {
	systray.SetTemplateIcon(icon, icon)
	systray.SetTooltip("tray concept - evaluating tray icon")

	showMenu := systray.AddMenuItem("Show", "Show the app console")
	hideMenu := systray.AddMenuItem("Hide", "Hide the app console")
	systray.AddSeparator()
	quitMenu := systray.AddMenuItem("Quit", "Quit the app")

	go func() {
		t := time.NewTicker(time.Second)
		for {
			select {
			case <-showMenu.ClickedCh:
				showConsole()

			case <-hideMenu.ClickedCh:
				hideConsole()

			case <-quitMenu.ClickedCh:
				log.Println("Requesting quit")
				systray.Quit()
				log.Println("Finished quitting")
				return

			case <-t.C:
				log.Println("tick-tock")
			}
		}
	}()
}

func onExit() {
	log.Println("exiting")
}
