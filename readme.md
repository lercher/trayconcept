# trayconcept

The idea is to link with `go build -ldflags "-H windows"` (and not `windowsapi`).
It has Windows create a console which captures the log we want, but we hide it
immediately with `ShowWindow(console-handle, 0)` so that it is invisible at startup.
It doesn't seem to flicker at least at my PC.

If we need the console, we show it with `ShowWindow(console-handle, 1)`
and can then hide it again.
